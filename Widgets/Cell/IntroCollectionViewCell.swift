//
//  IntroCollectionViewCell.swift
//  Widgets
//
//  Created by Imran on 18/12/2020.
//

import UIKit

class IntroCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var introIndexImageView: UIImageView!
    
}
