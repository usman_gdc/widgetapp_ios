//
//  UIView+Extension.swift
//  SeeSaw
//
//  Created by apple on 01/01/20.
//  Copyright © 2020 Sherry. All rights reserved.
//

import UIKit

extension UIView {
    /** Loads instance from nib with the same name. */
    func loadNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nibName = type(of: self).description().components(separatedBy: ".").last!
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
    }
}

extension UIView {
    func circle() {
        layer.cornerRadius = bounds.size.width/2
        layer.masksToBounds = true
    }
    
    func roundAndBorder() {
        layer.cornerRadius = bounds.size.width/2
        layer.masksToBounds = true
        layer.borderWidth = 2
        layer.borderColor = Colors.DarkColor?.cgColor
    }
    
   func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    func dropShadow() {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 2, height: 3)
        layer.masksToBounds = false

        layer.shadowOpacity = 0.2
        layer.shadowRadius = 2
        
        layer.rasterizationScale = UIScreen.main.scale
        layer.shouldRasterize = true
    }
    
    func dropRoundShadow( with radius: CGFloat) {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 2, height: 3)
        layer.masksToBounds = false

        layer.shadowOpacity = 0.2
        layer.shadowRadius = 2
        layer.cornerRadius = radius
        
        layer.rasterizationScale = UIScreen.main.scale
        layer.shouldRasterize = true
    }

    
    func roundCorner(){
        layer.cornerRadius = min(self.bounds.size.width/2, self.bounds.size.height/2)
        layer.masksToBounds = true
    }

    func roundCorner(_ radius: Int){
        layer.cornerRadius = CGFloat(radius)
        layer.masksToBounds = true
    }
}

class CircleShadowView: UIView {
    private var shadowLayer: CAShapeLayer!
    var fillColor: UIColor =  .white // the color applied to the shadowLayer, rather than the view's backgroundColor
     
    @IBInspectable open var cornerRadius: CGFloat = 20
    
    override func layoutSubviews() {
        super.layoutSubviews()
        backgroundColor = .clear
        
        if shadowLayer != nil {
            shadowLayer.removeFromSuperlayer()
            shadowLayer = nil
        }

        shadowLayer = CAShapeLayer()
          
        shadowLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
        shadowLayer.fillColor = fillColor.cgColor

        shadowLayer.shadowColor = UIColor.black.cgColor
        shadowLayer.shadowPath = shadowLayer.path
        shadowLayer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        shadowLayer.shadowOpacity = 0.2
        shadowLayer.shadowRadius = 3
            
        layer.insertSublayer(shadowLayer, at: 0)
    }
}

class TopCornerCircleShadowView: UIView {
    private var shadowLayer: CAShapeLayer!
    var fillColor: UIColor =  .white // the color applied to the shadowLayer, rather than the view's backgroundColor
     
    @IBInspectable open var cornerRadius: CGFloat = 20
    
    override func layoutSubviews() {
        super.layoutSubviews()
        backgroundColor = .clear
        
        if shadowLayer != nil {
            shadowLayer.removeFromSuperlayer()
            shadowLayer = nil
        }

        shadowLayer = CAShapeLayer()
          
        shadowLayer.path = UIBezierPath(roundedRect: bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius)).cgPath
        shadowLayer.fillColor = fillColor.cgColor

        shadowLayer.shadowColor = UIColor.black.cgColor
        shadowLayer.shadowPath = shadowLayer.path
        shadowLayer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        shadowLayer.shadowOpacity = 0.2
        shadowLayer.shadowRadius = 3
            
        layer.insertSublayer(shadowLayer, at: 0)
    }
}


class BottomCornerCircleShadowView: UIView {
    private var shadowLayer: CAShapeLayer!
    @IBInspectable open var fillColor: UIColor =  .white // the color applied to the shadowLayer, rather than the view's backgroundColor
     
    @IBInspectable open var cornerRadius: CGFloat = 20
    
    override func layoutSubviews() {
        super.layoutSubviews()
        backgroundColor = .clear
        
        if shadowLayer != nil {
            shadowLayer.removeFromSuperlayer()
            shadowLayer = nil
        }

        shadowLayer = CAShapeLayer()
          
        shadowLayer.path = UIBezierPath(roundedRect: bounds, byRoundingCorners: [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius)).cgPath
        shadowLayer.fillColor = fillColor.cgColor

        shadowLayer.shadowColor = UIColor.black.cgColor
        shadowLayer.shadowPath = shadowLayer.path
        shadowLayer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        shadowLayer.shadowOpacity = 1.2
        shadowLayer.shadowRadius = 10
            
        layer.insertSublayer(shadowLayer, at: 0)
    }
}

class RoundedView: UIView {
    @IBInspectable open var cornerRadius: CGFloat = 20
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = true
    }
}

class RoundedImageView: UIImageView {
    @IBInspectable open var cornerRadius: CGFloat = 20
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = true
    }
}

class CircleImageView: UIImageView {
    override func awakeFromNib() {
        image = UIImage(named: "placeholderImage")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = max(self.bounds.size.width,self.bounds.size.height)/2
        self.layer.masksToBounds = true
    }
}

//class ShadowView: UIView {
//    private var shadowLayer: CAShapeLayer!
//    private var fillColor: UIColor =  Colors.ShadowViewBgColor // .white // the color applied to the shadowLayer, rather than the view's backgroundColor
//
//    override func layoutSubviews() {
//        super.layoutSubviews()
//
//        backgroundColor = .clear
//
//        if shadowLayer == nil {
//
//            shadowLayer = CAShapeLayer()
//
//            shadowLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: 0).cgPath
//            shadowLayer.fillColor = fillColor.cgColor
//
//            shadowLayer.shadowColor = Colors.ShadowColor.cgColor
//            shadowLayer.shadowPath = shadowLayer.path
//            shadowLayer.shadowOffset = CGSize(width: 0.0, height: 1.0)
//            shadowLayer.shadowOpacity = 0.2
//            shadowLayer.shadowRadius = 3
//
//            layer.insertSublayer(shadowLayer, at: 0)
//        }
//    }
//}

//class FeaturedShadowView: UIView {
//    private var shadowLayer: CAShapeLayer!
//    private var fillColor: UIColor =  UIColor.init(red: 247.0/255.0, green: 202.0/255.0, blue: 24.0/255.0, alpha: 1.0)
//
//    override func layoutSubviews() {
//        super.layoutSubviews()
//
//        backgroundColor = .clear
//
//        if shadowLayer == nil {
//
//            shadowLayer = CAShapeLayer()
//
//            shadowLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: 5).cgPath
//            shadowLayer.fillColor = fillColor.cgColor
//
//            shadowLayer.shadowColor = Colors.ShadowColor.cgColor
//            shadowLayer.shadowPath = shadowLayer.path
//            shadowLayer.shadowOffset = CGSize(width: 0.0, height: 1.0)
//            shadowLayer.shadowOpacity = 0.2
//            shadowLayer.shadowRadius = 3
//
//            layer.insertSublayer(shadowLayer, at: 0)
//        }
//    }
//}

//class SolidColorShadowView: UIView {
//    private var shadowLayer: CAShapeLayer!
//    private var fillColor: UIColor =  Colors.PopUpColor // .white // the color applied to the shadowLayer, rather than the view's backgroundColor
//
//    override func layoutSubviews() {
//        super.layoutSubviews()
//
//        backgroundColor = .clear
//
//        if shadowLayer == nil {
//
//            shadowLayer = CAShapeLayer()
//
//            shadowLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: 0).cgPath
//            shadowLayer.fillColor = fillColor.cgColor
//
//            shadowLayer.shadowColor = Colors.ShadowColor.cgColor
//            shadowLayer.shadowPath = shadowLayer.path
//            shadowLayer.shadowOffset = CGSize(width: 0.0, height: 1.0)
//            shadowLayer.shadowOpacity = 0.2
//            shadowLayer.shadowRadius = 3
//
//            layer.insertSublayer(shadowLayer, at: 0)
//        }
//    }
//}
