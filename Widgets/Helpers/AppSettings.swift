//
//  AppSettingsViewController.swift
//  FashionAppTemplate
//
//  Created by apple on 06/03/20.
//  Copyright © 2020 iOSAppsWorld. All rights reserved.
//

import UIKit

public struct Constants{
    // ****************** in app purchase ids ******************//
    static let proProduct = "com.widget.pro"
    // ****************** in app purchase ids ******************//

    // ****************** App group Id ******************//
    static let appGroupId = "group.com.incubators.DailyQuotesWidget.calendarWidget"
    
    // ****************** Flicker image search key ******************//
    static let flickrKey = "3e7cc266ae2b0e0d7mefneke8e361736"

    // ****************** Unsplash image search key ******************//
    static let accessKey = "NZRpEpulbRDj-aZ4dtvdHRi1w45GtodmcnfcIs0hA"
    static let secretkey = "4jw7p5E4k6tUAOq03rMEeH-h1C7kfnekf0JGRJNfQQ"
    
    static let feedbackEmail = "youremail@gmail.com"

    static let moreAppUrl = "https://google.com"

    static let policyURL = "https://google.com"

    static let aboutURL = "https://google.com"

    static let shareAppMessage = "Hello, download the is awesome app"

}

public struct Colors{
    static let DarkColor = UIColor(named: "DarkColor")
    static let LightColor = UIColor(named: "LightColor")
    static let ThemeBlackColor = UIColor(named: "ThemeBlackColor")
}
 
public struct Fonts{
    static let RegularFont = UIFont(name: "Montserrat-Regular", size: 15)
    static let ExtraBoldFont = UIFont(name: "Montserrat-ExtraBold", size: 10)
    static let BoldItalicFont = UIFont(name: "Montserrat-BoldItalic", size: 10)
    static let BlackFont = UIFont(name: "Montserrat-Black", size: 10)
    static let MediumFont = UIFont(name: "Montserrat-Medium", size: 10)
    static let BoldFont = UIFont(name: "Montserrat-Bold", size: 15)
    static let LightFont = UIFont(name: "Montserrat-Light", size: 10)
    static let ThinFont = UIFont(name: "Montserrat-Thin", size: 15)
    static let SemiBoldFont = UIFont(name: "Montserrat-SemiBold", size: 15)
}
