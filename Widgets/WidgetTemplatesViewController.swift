//
//  WidgetTemplatesViewController.swift
//  Widgets
//
//  Created by Apple on 15/10/20.
//

import UIKit
import CoreData

typealias templateSelection = ((WidgetCollection) -> Void)?
class WidgetTemplatesViewController: UIViewController {
    @IBOutlet weak var templatesTable: UITableView!
    var sections:[String] = []
    var allTemplates:[TemplateWidget] = []
    var templateSelection:templateSelection!

    var pullUpControl: SOPullUpControl? {
         didSet {
             pullUpControl?.delegate = self
         }
     }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        templatesTable.backgroundColor = .clear
        templatesTable.backgroundView = UIView(frame: CGRect.zero)
        loadTemplates()

        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceivePurchaseNotif), name: NSNotification.Name(rawValue: "purchaseNotification"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func onDidReceivePurchaseNotif(){
        sections = []
        allTemplates = []
        loadTemplates()
    }
    
    func loadTemplates() {
        sections.append("Starter")
        sections.append("Pro")
        sections.append("Dark")
        sections.append("Love")
        sections.append("Light")
        sections.append("Color")
        sections.append("Gardient")
        sections.append("City")
        sections.append("Sports")
        sections.append("Galaxy")

        let isPro = UserDefaults.standard.bool(forKey: "isPro")
        
        for section in sections{
            var templates = getTemplates(of: section)
            templates = templates.map({ (template) -> TemplateWidget in
                template.type = section
                if section == "Pro" && isPro == false{
                    template.isPro = 1
                }
                else{
                    template.isPro = 0
                }
                return template
            })
            allTemplates.append(contentsOf: templates)
        }
        templatesTable.reloadData()
    }
    
    func getTemplates(of type:String) -> [TemplateWidget]{
        var templates:[TemplateWidget] = []
        if let path = Bundle.main.path(forResource: type, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let templatesList = jsonResult as? [[String:Any]]{
                    for template in templatesList{
                        let templateObj = TemplateWidget(json: template)
                        templates.append(templateObj)
                    }
                }
            }catch {
                // handle error
           }
        }
        
        return templates
    }
}

extension WidgetTemplatesViewController:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 210
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 40))
        view.backgroundColor = .clear
        let label = BoldLabel(frame: CGRect(x: 20, y: 0, width: UIScreen.main.bounds.size.width - 170, height: 40))
        label.textColor = Colors.ThemeBlackColor
        label.font = UIFont(name: Fonts.BoldFont!.fontName, size: 15)
        label.text = sections[section].capitalizingFirstLetter()
            
        view.addSubview(label)
            
        return view
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TemplateGroupTableViewCell.identifier, for: indexPath) as! TemplateGroupTableViewCell
        let templates = allTemplates.filter({$0.type == sections[indexPath.section]})
        cell.templateCollection.templateWidgets = templates
        cell.templateCollection.reloadData()
        return cell
    }
}

extension WidgetTemplatesViewController: UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        guard let collectionView  =  collectionView as? TemplateCollectionView else {
            return 0
        }
        return collectionView.templateWidgets.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let collectionView  =  collectionView as? TemplateCollectionView else { return UICollectionViewCell() }

        let template = collectionView.templateWidgets[indexPath.section]

//        if template.type == WidgetType.calendar.rawValue{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CalendarTemplateCollectionViewCell.identifier, for: indexPath) as? CalendarTemplateCollectionViewCell
            
//            let calendarTemplate = (template as? TemplateWidget)!
            cell?.setItem(template, sizeType: "medium", canDelete: false)
            
            return cell!
//        }
//        else if template.type == WidgetType.reminder.rawValue{
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ReminderTemplateCollectionViewCell.identifier, for: indexPath) as? ReminderTemplateCollectionViewCell
//
////            let calendarTemplate = (template as? TemplateWidget)!
//            cell?.setItem(template, sizeType: "medium", canDelete: false)
//            return cell!
//        }
//        else{
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: QuoteTemplateCollectionViewCell.identifier, for: indexPath) as? QuoteTemplateCollectionViewCell
//
////            let calendarTemplate = (template as? TemplateWidget)!
//            cell?.setItem(template, sizeType: "medium", canDelete: false)
//            return cell!
//        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let collectionView  =  collectionView as? TemplateCollectionView else { return }

        let template = collectionView.templateWidgets[indexPath.section]
        
        if template.isPro == 1{
            let purchaseView = self.storyboard?.instantiateViewController(withIdentifier: "ManagePurchaseViewController") as? ManagePurchaseViewController
            present(purchaseView!)
        }
        else{
            self.templateSelection!(template.toWidgetCollection())
        }
    }
}

extension WidgetTemplatesViewController:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: UIScreen.main.bounds.size.width - 50, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension WidgetTemplatesViewController:SOPullUpViewDelegate{
    func pullUpViewStatus(_ sender: UIViewController, didChangeTo status: PullUpStatus) {
        
    }
    
    func pullUpHandleArea(_ sender: UIViewController) -> UIView {
        return self.view
    }
}
