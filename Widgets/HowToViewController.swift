//
//  HowToViewController.swift
//  Widgets
//
//  Created by Apple on 23/10/20.
//

import UIKit


struct IntoScreen {
    var descriptionInfo : String!
    var imageName : String!
    
    init(descriptionInfo : String, imageName : String) {
        self.descriptionInfo = descriptionInfo
        self.imageName = imageName
    }
}

class HowToViewController: UIViewController , UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{

    @IBOutlet weak var introCollectionView : UICollectionView!
    @IBOutlet weak var introPageControl: UIPageControl!
    @IBOutlet weak var skipButtonContainerView: UIView!
    var arrayOfIntro : [IntoScreen] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        arrayOfIntro.append(IntoScreen(descriptionInfo: "Long press the home screen to edit, then tap the "+" button in the top left corner of the screen", imageName: "intro_1"))
        arrayOfIntro.append(IntoScreen(descriptionInfo: "Scroll down and select Photo Widget", imageName: "intro_2"))
        arrayOfIntro.append(IntoScreen(descriptionInfo: "Choose a widget that looks like you want and add it to your home screen", imageName: "intro_3"))
        arrayOfIntro.append(IntoScreen(descriptionInfo: "To edit / delete / move the widget, long press the added widget.", imageName: "intro_4"))
        introCollectionView.delegate = self
        introCollectionView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        skipButtonContainerView.layer.cornerRadius = skipButtonContainerView.bounds.height/2
        skipButtonContainerView.clipsToBounds = true
    }
    
    @IBAction func skipTutorialButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayOfIntro.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IntroCollectionViewCell", for: indexPath) as! IntroCollectionViewCell
        let introObj = arrayOfIntro[indexPath.row]
        cell.descriptionLabel.text = introObj.descriptionInfo
        cell.introIndexImageView.image = UIImage(named: introObj.imageName)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: introCollectionView.bounds.width, height: introCollectionView.bounds.height)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        for cell in introCollectionView.visibleCells {
            let indexPath = introCollectionView.indexPath(for: cell)
            introPageControl.currentPage = indexPath?.row ?? 0
        }
    }


}
