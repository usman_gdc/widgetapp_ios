//
//  ManagePurchaseViewController.swift
//  FMPhotoPickerExample
//
//  Created by Apple on 14/09/20.
//  Copyright © 2020 Tribal Media House. All rights reserved.
//

import UIKit
import StoreKit

struct SettingsStruct {
    var title : String!
    
    init(title : String){
        self.title = title
    }
}

class ManagePurchaseViewController: UIViewController {
    @IBOutlet weak var subscriptionPriceLabel: UILabel!
    @IBOutlet weak var subscribeButton: UIButton!
    @IBOutlet weak var purchaseManagerTableView: UITableView!
    var dataForFirstSectionSettings : [SettingsStruct] = []
    var dataForSecondSectionSettings : [SettingsStruct] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupData()
        subscribeButton.roundCorner(5)
        subscribeButton.setTitle("Buy in \(PurchaseManager.shared.proProduct?.localizedPrice ?? "0")", for: .normal)
        
        purchaseManagerTableView.delegate = self
        purchaseManagerTableView.dataSource = self
        purchaseManagerTableView.tableFooterView = UIView()
    }
    
    func setupData(){
        //First Section
        dataForFirstSectionSettings.append(SettingsStruct(title: "Tutorial"))
        dataForFirstSectionSettings.append(SettingsStruct(title: "Contact"))
        
        //Second Section
        dataForSecondSectionSettings.append(SettingsStruct(title: "Rate App"))
        dataForSecondSectionSettings.append(SettingsStruct(title: "Feedback"))
        dataForSecondSectionSettings.append(SettingsStruct(title: "Privacy Policy"))
        dataForSecondSectionSettings.append(SettingsStruct(title: "Terms of Use"))
        dataForSecondSectionSettings.append(SettingsStruct(title: "More Apps"))
    }

    @IBAction func continueBtnClicked(){
        showLoaderView()
        PurchaseManager.shared.purchaseProduct(Constants.proProduct, success: {
            UserDefaults.standard.setValue(true, forKey: "isPro")
            UserDefaults.standard.synchronize()
            self.hideLoaderView()
            self.showMessage(message: "Congratulation, you have unlocked the pro version", header: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "purchaseNotification"), object: nil)

        }) { (error) in
            self.hideLoaderView()
            self.showMessage(message: error!, header: nil)
        }
    }
    
    @IBAction func shareBtnClicked(){
        let items = [Constants.shareAppMessage]
        let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
        present(ac, animated: true)
    }
    
    @IBAction func rateBtnClicked(){
        if let scene = UIApplication.shared.currentScene {
            SKStoreReviewController.requestReview(in: scene)
        }
    }
    
    @IBAction func policyBtnClicked(){
        if let url = URL(string: Constants.policyURL) {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func aboutBtnClicked(){
        if let url = URL(string: Constants.aboutURL) {
            UIApplication.shared.open(url)
        }
    }
}

extension ManagePurchaseViewController : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "INFORMATION"
        } else if section == 1{
            return "SUPPORT"
        }
        return ""
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return dataForFirstSectionSettings.count
        } else if section == 1{
            return dataForSecondSectionSettings.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsTableViewCell", for: indexPath) as! SettingsTableViewCell
        if indexPath.section == 0 {
            cell.titleLabel.text = dataForFirstSectionSettings[indexPath.row].title
        } else if indexPath.section == 1 {
            cell.titleLabel.text = dataForSecondSectionSettings[indexPath.row].title
        }
        
        
        return cell
    }
}
