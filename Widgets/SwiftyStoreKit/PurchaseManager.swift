//
//  PurchaseManager.swift
//  UnivarsalLearningapp
//
//  Created by apple on 22/04/20.
//  Copyright © 2020 iOSAppsWorld. All rights reserved.
//

import UIKit
import StoreKit

typealias failure = (_ errorMessage: String?) -> Void

class PurchaseManager {
    static let shared = PurchaseManager()
    private init(){}
    var proProduct: SKProduct?

    func initialize(){
        SwiftyStoreKit.setupIAP()
        
        SwiftyStoreKit.retrieveProductsInfo([Constants.proProduct]) { result in
            DispatchQueue.main.async {
                if result.retrievedProducts.count > 0{
                    for product in result.retrievedProducts{
                        switch product.productIdentifier {
                        case Constants.proProduct:
                            self.proProduct = product
                       
                        default:
                            print("default")
                        }
                    }
                }
            }
        }
    }

    func restorePurchase(_ success: @escaping () -> (), failure: @escaping failure) {
        SwiftyStoreKit.restorePurchases { (products) in
            DispatchQueue.main.async {
                for product in products.restoredPurchases{
                    if product.productId == Constants.proProduct{
                        UserDefaults.standard.set(true, forKey: Constants.proProduct)
                        UserDefaults.standard.synchronize()
                    }
                }
                success()
            }
        }
    }
    
    func purchaseProduct(_ productId: String,success: @escaping () -> (), failure: @escaping failure)  {
        success()
        return;
        SwiftyStoreKit.purchaseProduct(productId, quantity: 1, atomically: true) { result in
        DispatchQueue.main.async {
            switch result {
            case .success(let purchase):
                switch purchase.productId {
                case Constants.proProduct:
                    UserDefaults.standard.set(true, forKey: Constants.proProduct)
                    UserDefaults.standard.synchronize()
                default:
                    print("default")
                }
                success()
                                
            case .error(let error):
                switch error.code {
                case .unknown: failure( "Unknown error. Please try again later")
                case .clientInvalid: failure("Not allowed to make the payment")
                case .paymentCancelled: failure("Payment Canceled")
                case .paymentInvalid: failure("The purchase identifier was invalid")
                case .paymentNotAllowed: failure("The device is not allowed to make the payment")
                case .storeProductNotAvailable: failure("The product is not available in the current storefront")
                case .cloudServicePermissionDenied: failure("Access to cloud service information is not allowed")
                case .cloudServiceNetworkConnectionFailed: failure("Could not connect to the network")
                case .cloudServiceRevoked: failure("User has revoked permission to use this cloud service")
                default:
                    failure("Unknown error. Please try again later")
                }
            }
            }
        }
    }
}
